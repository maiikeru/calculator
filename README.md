# Projekt: Kalkulačka s historii

#Zadání:
kalkulačka se zobrazením historie výpočtů, která bude vypadat a fungovat podobně, jako obyčejná kupecká kalkulačka, která tiskne historii na papírovou pásku. Aplikaci můžete napsat v libovolném jazyce a libovolném grafickém prostředí. 

#Implementace
- Pro implemtaci byl zvolen jazyk Python 2.7 a pro grafické prostředí knihovna Tkinter. 
- Aplikace umí provádět výpočty s operátory "+, -, *, /, %, (, )".
- Je také ošetřen stav při dělení nulou.  Na vstup se vytiskne chybová hláška "Error". 
- Při stiknutí znaku "=" se provede výpočet, který se zapisuje na vstup a je také tisknuta historie výpočtu na výstup příkazové řádky.
- Aplikace dále umožnuje zobrazení minulých výpočtů.

#Ukázka

Aplikace gui:

![Alt text](33.png "Kalkulacka ve stavu přepínání minulých výpočtů")

Papírová páska:

.1+.2-.3= 0.0

1+1= 2.0

3*3= 9.0

9.0*3= 27.0

27.0*3= 81.0

81.0*3= 243.0

5*(2-3*4)= -50.0
