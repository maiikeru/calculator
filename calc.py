#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This is a calculator with time history of result"""

from __future__ import division
from Tkinter import *

__author__ = "Michal Jurca"
__version__ = "1.0.3"
__email__ = "jurca.michal@gmail.com"
__status__ = "Done"


class App:
    def __init__(self, master):
        self.master = master
        self.master.title("Calculator")
        self.master.resizable(width=False, height=False)

        self.entry_value = StringVar(self.master, value="")
        # -------  0 row  -------
        self.window_text = Text(self.master, font=("Arial", 12), width=10, height=10, bd=0, bg="light grey")
        self.window_text.grid(row=0, columnspan=5, sticky="NWNESWSE", rowspan=1)
        self.window_text.config(state=DISABLED)

        # -------  1 row  -------
        self.entry_text = Entry(self.master, font=("Arial", 16), textvariable=self.entry_value, relief=RAISED,
                                justify=RIGHT, bd=0)
        self.entry_text.grid(row=1, columnspan=5)

        # -------  2 row  -------
        self.button7 = Button(self.master, font=("Arial", 12), text="7", command=lambda: self.num_btn_press("7"), bd=0,
                              bg="white")
        self.button7.grid(row=2, column=0, sticky="NWNESWSE")

        self.button8 = Button(self.master, font=("Arial", 12), text="8", command=lambda: self.num_btn_press("8"), bd=0,
                              bg="white")
        self.button8.grid(row=2, column=1, sticky="NWNESWSE")

        self.button9 = Button(self.master, font=("Arial", 12), text="9", command=lambda: self.num_btn_press("9"), bd=0,
                              bg="white")
        self.button9.grid(row=2, column=2, sticky="NWNESWSE")

        self.button_add = Button(self.master, font=("Arial", 12), text="+", command=lambda: self.op_btn_press("+"),
                                 bd=0, bg="grey")
        self.button_add.grid(row=2, column=3, sticky="NWNESWSE")

        self.button_clear = Button(self.master, font=("Arial", 12), text="C", command=lambda: self.clear_btn_press(),
                                   bd=0,
                                   bg="red")
        self.button_clear.grid(row=2, column=4, sticky="NWNESWSE")

        # -------  3 row  -------
        self.button4 = Button(self.master, font=("Arial", 12), text="4", command=lambda: self.num_btn_press("4"), bd=0,
                              bg="white")
        self.button4.grid(row=3, column=0, sticky="NWNESWSE")

        self.button5 = Button(self.master, font=("Arial", 12), text="5", command=lambda: self.num_btn_press("5"), bd=0,
                              bg="white")
        self.button5.grid(row=3, column=1, sticky="NWNESWSE")

        self.button6 = Button(self.master, font=("Arial", 12), text="6", command=lambda: self.num_btn_press("6"), bd=0,
                              bg="white")
        self.button6.grid(row=3, column=2, sticky="NWNESWSE")

        self.button_sub = Button(self.master, font=("Arial", 12), text="-", command=lambda: self.op_btn_press("-"),
                                 bd=0, bg="grey")
        self.button_sub.grid(row=3, column=3, sticky="NWNESWSE")

        self.button_bracket_l = Button(self.master, font=("Arial", 12), text="(",
                                       command=lambda: self.op_btn_press("("), bd=0, bg="grey")
        self.button_bracket_l.grid(row=3, column=4, sticky="NWNESWSE")

        # -------  4 row  -------
        self.button1 = Button(self.master, font=("Arial", 12), text="1", command=lambda: self.num_btn_press("1"),
                              bd=0, bg="white")
        self.button1.grid(row=4, column=0, sticky="NWNESWSE")

        self.button2 = Button(self.master, font=("Arial", 12), text="2", command=lambda: self.num_btn_press("2"), bd=0,
                              bg="white")
        self.button2.grid(row=4, column=1, sticky="NWNESWSE")

        self.button3 = Button(self.master, font=("Arial", 12), text="3", command=lambda: self.num_btn_press("3"), bd=0,
                              bg="white")
        self.button3.grid(row=4, column=2, sticky="NWNESWSE")

        self.button_mult = Button(self.master, font=("Arial", 12), text="*", command=lambda: self.op_btn_press("*"),
                                  bd=0, bg="grey")
        self.button_mult.grid(row=4, column=3, sticky="NWNESWSE")

        self.button_bracket_r = Button(self.master, font=("Arial", 12), text=")",
                                       command=lambda: self.op_btn_press(")"), bd=0, bg="grey")
        self.button_bracket_r.grid(row=4, column=4, sticky="NWNESWSE")

        # -------  5 row  -------
        self.button0 = Button(self.master, font=("Arial", 12), text="0", command=lambda: self.num_btn_press("0"), bd=0,
                              bg="white")
        self.button0.grid(row=5, column=0, sticky="NWNESWSE")

        self.button_dot = Button(self.master, font=("Arial", 12), text=".", command=lambda: self.dot_btn_press("."),
                                 bd=0, bg="white")
        self.button_dot.grid(row=5, column=1, sticky="NWNESWSE")

        self.button_per = Button(self.master, font=("Arial", 12), text="%", command=lambda: self.op_btn_press("%"),
                                 bd=0, bg="grey")
        self.button_per.grid(row=5, column=2, sticky="NWNESWSE")

        self.button_div = Button(self.master, font=("Arial", 12), text="/", command=lambda: self.op_btn_press("/"),
                                 bd=0, bg="grey")
        self.button_div.grid(row=5, column=3, sticky="NWNESWSE")

        self.button_egual = Button(self.master, font=("Arial", 12), text="=", bd=0, bg="orange",
                                   command=lambda: self.equal_btn_press())
        self.button_egual.grid(row=5, column=4, sticky="NWNESWSE")

    # Equal button pressed
    equal_trigger = False
    # Dot button pressed
    dot_trigger = False
    # Operator button trigger
    op_trigger = False
    op_now_trigger = False
    # Right bracelet trigger
    bracelet_r_trigger = False
    # Save last num
    tmp_num = "A"
    # Save last pressed operator
    str_operator_trigger = ""
    # Save last correct result
    str_tmp_result = ""

    # Rewrite entry with value
    def rewrite_entry(self, src):
        self.entry_text.delete(0, END)
        self.entry_text.insert(0, src)

    # Write result history to first windows
    # http://stackoverflow.com/questions/3842155/is-there-a-way-to-make-the-tkinter-text-widget-read-only
    def write_history(self, text):
        self.window_text.config(state=NORMAL)
        self.window_text.insert(END, text + '\n')
        self.window_text.config(state=DISABLED)

    # Number button pressed
    def num_btn_press(self, _value):
        str_bracet = ""
        value = ""
        # Format char "(" into "*("
        if (self.tmp_num.isdigit() or self.tmp_num == "." or self.tmp_num == ")") and _value == "(":
            value = "*" + _value
            self.equal_trigger = False
        # Format ")" into ")*"
        elif self.bracelet_r_trigger and (_value.isdigit() or _value == "."):
            # Correct )+6, wrong )+*6
            if self.tmp_num == "+" or self.tmp_num == "-" or self.tmp_num == "*" or self.tmp_num == "/" or self.tmp_num == "%" or self.tmp_num == "(":
                value = _value
            # Correct )*6
            else:
                if self.tmp_num == ")" and _value == ".":
                    str_bracet = "*"
                else:
                    value = "*" + _value

            self.op_trigger = False
            self.bracelet_r_trigger = False
        else:
            value = _value

        # Get actual value in entry
        digits = self.entry_text.get()

        # Composed of digits to the right form
        if self.equal_trigger and (_value.isdigit() or _value == "."):
            self.equal_trigger = False
            digits = value
        # Correct 0.1, wrong 0.0.001..
        elif self.dot_trigger and _value == ".":
            digits += ""
        elif _value == "." and not self.tmp_num.isdigit():
            digits += str_bracet + "0."
        # Correct 5+7-, wrong 5+/*-+/+/65+/
        elif self.op_trigger and (_value == "+" or _value == "-" or _value == "*" or _value == "/" or _value == "%"):
            char = digits[-1:]
            # Correct +, +- -> -, Wrong +-+-++-+/*+/
            if self.op_now_trigger and (str(char).isdigit() or char == "(" or char == ")" or char == "."):
                digits += _value
                self.op_now_trigger = False
            # Correct )+, 3*3; Wrong )+ -> +, *3
            elif (self.bracelet_r_trigger or self.tmp_num.isdigit()) and not self.op_now_trigger:
                digits += _value
            else:
                digits = digits[:-1]
                digits += _value
            self.op_now_trigger = True
            self.op_trigger = False

        else:
            digits += value

        # Write digits to entry
        self.rewrite_entry(digits)
        # Save actual digit
        self.tmp_num = _value

    # Operator button pressed
    def op_btn_press(self, value):
        if value == "%":
            self.str_operator_trigger = "*"
        elif value == ")":
            self.bracelet_r_trigger = True
        else:
            self.str_operator_trigger = value
        self.dot_trigger = False
        self.equal_trigger = False
        self.num_btn_press(value)
        self.op_trigger = True

    def dot_btn_press(self, _value):
        self.num_btn_press(_value)
        self.dot_trigger = True

    # Check bracelet in expresion. Return string  expresion with add bracelet or Error code if wrong form
    def check_bracelet(self, text):
        left = text.count("(")
        right = text.count(")")

        if left != right:
            # Add missing ) into end
            if left > right:
                output = text
                while 0 < left - right:
                    output += ")"
                    right += 1
            # Wrong count bracelet
            else:
                output = text + "Error"
        # Correct
        else:
            output = text

        return output

    # Compute multiple pressed equal. Return string of result.
    def compute_multiple_equal(self, text):
        start = text.rfind(str(self.str_operator_trigger))
        stop = text.find("=")
        return text[stop + 2:] + text[start:stop]

    # Equal button presed. Function calculates and write result to entry
    def equal_btn_press(self):
        # Get value from entry
        str_entry_val = self.entry_value.get()
        str_entry_val = str_entry_val.replace("%", "/100*")

        str_entry_val = self.check_bracelet(str_entry_val)

        if self.equal_trigger:
            str_entry_val = self.compute_multiple_equal(self.str_tmp_result)

        try:
            # Compute expression and format float point mumber
            # result = str(eval(str_entry_val))
            result = round(eval(str_entry_val), 12)
            # Write result to entry
            self.rewrite_entry(result)
            # Make expression
            srt_result = str(str_entry_val) + "= " + str(result)
            # Set true if equal button pressed
            self.equal_trigger = True
            # Set tmp result
            self.str_tmp_result = srt_result

        except:
            self.rewrite_entry("Error, invalid input.")
            srt_result = str_entry_val + "= Error"
            # Set default value
            self.tmp_num = "A"

        # Write result to history reselts
        self.write_history(srt_result)
        # Print receipt to stdout
        print srt_result

    # Erase entry
    def clear_btn_press(self):
        self.rewrite_entry("")
        # Set default value
        self.tmp_num = "A"


if __name__ == "__main__":
    root = Tk()
    app = App(root)
    root.mainloop()